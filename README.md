# Shaanti

A [Shaarli](https://github.com/shaarli/Shaarli) browser extension using the API for both Firefox and Chrome based browsers.
It features add/edit and search of bookmarks for your Shaarli instance.

<img src="./shaati_screenshot.jpg" alt="Extension screenshot" width="100%" align="center" />

## Download


[Firefox Browser add-ons](https://addons.mozilla.org/firefox/addon/shaanti/)

[Chrome web store](https://chromewebstore.google.com/detail/shaanti/bfecpppjnokkpdegijfgbldholankami)

Alternative use the packaged extensions in the [`./build/`](https://codeberg.org/BinaryUnit/Shaanti/src/branch/main/build) folder.

## Config

Note: "Enable REST API" option needs to be set to **on** in your instance

- add your instance url in the format `some.domain.com`
- add your REST API secret from your instance
- optional configure extension shortcuts within the browser

## Development

`npm run watch`

**Warning:** [inotify-tools](https://github.com/inotify-tools/inotify-tools/wiki) needs to be installed.\
It will rsync the src folder to both `./extension_firefox/` and `./extension_chrome/` directories, with the exception of `manifest.json` that is platform specific.

`npm run build`

Using `web-ext` zips both extension folders in the [`./build/`](https://codeberg.org/BinaryUnit/Shaanti/src/branch/main/build) directory


## Credits
[js-sha512](https://github.com/emn178/js-sha512)\
[Tagify](https://github.com/yairEO/tagify)\
[Icons](https://feathericons.com/)\
[Shaarli](https://github.com/shaarli/Shaarli)