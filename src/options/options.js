import { ShaarliApi } from '../assets/js/shaarli.js'
import { UtilsShaanti } from '../assets/js/utils_shaanti.js'

document.addEventListener("DOMContentLoaded", () => {
  let shaarliApi;
  let utilsShaanti = new UtilsShaanti();
  let $optionInstance = document.querySelector('.js-ext-options-instance');
  let $optionToken = document.querySelector('.js-ext-options-token');
  let $optionPrivate = document.querySelector('.js-ext-options-private');

  let runTime = (typeof browser === 'undefined') ? chrome : browser;

  document.querySelector('.js-ext-options-form').addEventListener('submit', formOptionsSubmit);
  $optionPrivate.addEventListener('change', changePrivate);

  async function formOptionsSubmit(e) {
    e.preventDefault();
    utilsShaanti.addLoadingSave();

    const vInstanceDomain = normalizeDomain($optionInstance.value);
    const vToken = $optionToken.value;

    $optionInstance.value = vInstanceDomain;

    if (!vInstanceDomain.length > 0 || !vToken.length > 0) {
      utilsShaanti.removeLoadingSave();
      utilsShaanti.showToast(false, 'Error!! Instance url and API key must be filled');
      return false;
    }

    let vInstanceDomainUrl = `https://${vInstanceDomain}`;
    try {
      vInstanceDomainUrl = new URL(vInstanceDomainUrl);
      vInstanceDomainUrl = vInstanceDomainUrl.hostname;
    } catch(error) {
      utilsShaanti.removeLoadingSave();
      utilsShaanti.showToast(false, 'Error!! Malformed url please use some.domain.com or some.domain.com/path format');
      return false
    }

    try {
      const perms = await runTime.permissions.request({
        'origins': [`*://${vInstanceDomainUrl}/*`]
      });

      const currentPermissions = await onResponsePermissions(perms);
      // console.log(`Current permissions:`, currentPermissions);

      if (currentPermissions?.origins.includes(`*://${vInstanceDomainUrl}/*`) || currentPermissions?.origins.includes(`<all_urls>`)) {
        utilsShaanti.showToast(true, `Success!! Permissions for "${vInstanceDomainUrl}" granted`);
      } else {
        utilsShaanti.showToast(false, `Error!! Permissions for "${vInstanceDomainUrl}" must be granted`);
        utilsShaanti.removeLoadingSave();
        return false
      }

    } catch (error) {
      utilsShaanti.showToast(false, `Error!! ${error}`);
      utilsShaanti.removeLoadingSave();
      console.log(error);
      return false
    }

    shaarliApi = new ShaarliApi(vInstanceDomain, vToken);

    try {
      const data = await shaarliApi.endpoint('info', 'get');

      // save data if there are no errors
      if (!data.error && data.settings) {
        runTime.storage.sync.set({
          // instanceUrl is called Url due to legacy reasons
          // so that it does not break for past users
          instanceUrl: vInstanceDomain,
          token: vToken,
          defaultPrivateLinks: data.settings.default_private_links
        });
        $optionPrivate.checked = data.settings.default_private_links;
        utilsShaanti.showToast(true, 'Success!! You can start using the extension');
      } else {
        utilsShaanti.showToast(false, `Error!! Could not connect to instance.\n\n"${data.error}"`);
      }
      utilsShaanti.removeLoadingSave();

    } catch (error) {
      console.log(error);
      utilsShaanti.removeLoadingSave();
      utilsShaanti.showToast(false, `Error!! Could not connect to instance.\n\n"${error}"`);
    }

  };

  function onResponsePermissions(response) {
    if (response) {
      console.log("Permission was granted");
    } else {
      console.log("Permission was refused");
    }
    return runTime.permissions.getAll();
  }

  function normalizeDomain(url) {
    url = url.replace('http://', '')
      .replace('https://', '')
      // removes / from beginning and end of string
      .replace(/(^\/+|\/+$)/mg, '');
    return url;
  }



  function changePrivate(e) {
    runTime.storage.sync.set({
      defaultPrivateLinks: e.target.checked
    });
  };


  function restoreOptions() {
    function setCurrentChoice(result) {
      $optionInstance.value = result.instanceUrl || '';
      $optionToken.value = result.token || '';
      $optionPrivate.checked = result.defaultPrivateLinks
    }

    function onError(error) {
      console.log(`Error: ${error}`);
    }

    let getting = runTime.storage.sync.get(['instanceUrl', 'token', 'defaultPrivateLinks']);
    getting.then(setCurrentChoice, onError);
  }

  restoreOptions();
});
