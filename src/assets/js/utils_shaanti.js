export class UtilsShaanti {
  $toast;
  $btnLoad;
  $$tabButtons;

  constructor() {

    this.$toast = document.querySelector('.js-toast');
    this.$btnLoad = document.querySelector('.js-btn-save');
    this.$$tabButtons = document.querySelectorAll('.js-tab-button');

    this.$toast.querySelector('button').addEventListener('click', (e) => {
      e.preventDefault();
      e.target.parentElement.classList.add('hide');
    })

    this.$$tabButtons.forEach(($e) => $e.addEventListener('click', this.switchTab))

  }

  showToast(success = true, message) {
    if (success) {
      this.$toast.classList.add('success');
      this.$toast.classList.remove('error');
      this.$toast.classList.remove('hide');
    } else {
      this.$toast.classList.add('error');
      this.$toast.classList.remove('success');
      this.$toast.classList.remove('hide');
    }
    this.$toast.querySelector('.js-message').textContent = message;
  }

  addLoadingSave() {
    this.$btnLoad.style.height = `${this.$btnLoad.offsetHeight}px`;
    this.$btnLoad.querySelector(':first-child').classList.add('hide');
    this.$btnLoad.querySelector(':last-child').classList.remove('hide');
    this.$btnLoad.disabled = true;
  }

  removeLoadingSave() {
    this.$btnLoad.querySelector(':first-child').classList.remove('hide');
    this.$btnLoad.querySelector(':last-child').classList.add('hide');
    this.$btnLoad.disabled = false;
  }

  switchTab(e) {
    const tabName = e.currentTarget.dataset?.tabname;
    document.querySelectorAll('.js-tab-button').forEach($e => $e.classList.remove('current-tab'));
    document.querySelector(`a[data-tabname="${tabName}"]`).classList.add('current-tab');
    document.querySelectorAll('.js-tab').forEach($e => $e.classList.add('hide'));
    document.querySelector(`.js-tab[data-tabname="${tabName}"]`).classList.remove('hide');
    if (tabName == 'search') {
      document.querySelector('.js-popup-header').querySelector('.js-header-title').textContent = 'Search';
      document.querySelector('.js-search-input').focus();
    } else {
      document.querySelector('.js-popup-header').querySelector('.js-header-title').textContent = 'Add/Edit';
    }
  }

}