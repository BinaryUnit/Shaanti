export class ShaarliApi {
  #instanceUrl
  #apiSecret
  #endpoints

  constructor(instanceUrl, apiSecret) {
    if (!(instanceUrl && apiSecret)) {
      throw new Error("Missing arguments [apiUrl, apiSecret]")
    }
    this.instanceUrl = instanceUrl;
    this.apiSecret = apiSecret;
    this.apiUrl = `${instanceUrl}/api/v1`;

    this.endpoints = {
      links: {
        search: (options = {}) => {
          return {
            method: 'GET',
            resource: 'links',
            params: {
              "limit": 1,
              "searchterm": "string",
              ...options
            }
          }
        },
        edit: (options = {}) => {
          return {
            method: 'PUT',
            resource: 'links/:id',
            params: {
              ...options
            }
          }
        },
        create: (options = {}) => {
          return {
            method: 'POST',
            resource: 'links',
            params: {
              ...options
            }
          }
        }
      },
      tags: {
        list: (options = {}) => {
          return {
            method: 'GET',
            resource: 'tags',
            params: {
              "offset": 0,
              "limit": "all",
              "visibility": "all",
              ...options
            }
          }
        },
      },
      info: {
        get: (options = {}) => {
          return {
            method: 'GET',
            resource: 'info',
            params: {
              ...options
            }
          }
        },
      }

    }
  }

  queryBuilder(params) {
    // return new URLSearchParams(params).toString();
    return Object.keys(params).map(key => {
      if (key === 'type[]' || key === 'status[]') {
        return params[key].split(',').map((t) => key + '=' + t.trim()).join('&')
      }
      return key + '=' + params[key]
    }).join('&');
  }

  rtrim(str, charlist) {
    charlist = !charlist
      ? ' \\s\u00A0'
      : (charlist + '').replace(/([[\]().?/*{}+$^:])/g, '\\$1')
    const re = new RegExp('[' + charlist + ']+$', 'g')
    return (str + '').replace(re, '')
  }

  base64url_encode($data) {
    let repl = btoa($data);
    repl = repl.replaceAll('+', '-');
    repl = repl.replaceAll('/', '_');
    return this.rtrim(repl, '=');
  }


  ArrayBufferToString(buffer) {
    return this.BinaryToString(String.fromCharCode.apply(null, Array.prototype.slice.apply(new Uint8Array(buffer))));
  }

  BinaryToString(binary) {
    var error;

    try {
      return decodeURIComponent(escape(binary));
    } catch (_error) {
      error = _error;
      if (error instanceof URIError) {
        return binary;
      } else {
        throw error;
      }
    }
  }


  generateToken(secret) {
    const ctime = Math.floor(Date.now() / 1000);
    const header = this.base64url_encode('{"typ": "JWT","alg": "HS512"}');
    const payload = this.base64url_encode(`{"iat": ${ctime - 60}, "exp": ${ctime + 120}}`);
    const hash = sha512.hmac.create(secret);
    hash.update(`${header}.${payload}`);
    hash.hex();
    const signature = this.base64url_encode(this.ArrayBufferToString(hash.arrayBuffer()));
    return `${header}.${payload}.${signature}`;
  }

  getMessageEncoding = (m) => {
    let enc = new TextEncoder();
    return enc.encode(m);
  };

  async request(endpoint) {
    let qparams = '';
    let options = {};
    const token = this.generateToken(this.apiSecret);

    if (endpoint?.resource.includes(':id') && endpoint?.params?.id !== 'undefined') {
      endpoint.resource = endpoint.resource.replace(/:id/g, endpoint?.params.id);
      delete (endpoint?.params.id);
    }


    if (endpoint?.method == 'GET') {
      qparams = '?' + new URLSearchParams(endpoint?.params).toString()
    }

    if (endpoint?.method == 'POST' || endpoint?.method == 'PUT') {
      options.body = JSON.stringify(endpoint?.params);
    }

    return fetch(`https://${this.instanceUrl}/api/v1/${endpoint.resource}${qparams}`, {
      ...options,
      method: endpoint?.method,
      // mode: 'no-cors',
      // credentials changed from include to omit probably how should have been in the first place
      credentials: 'omit',
      cache: 'no-cache',
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token}`,
        // https://developer.mozilla.org/en-US/docs/Glossary/Forbidden_header_name
        "Access-Control-Request-Headers": "X-Requested-With, Content-Type, Accept, Origin, Authorization"

      }
    }).then(async (response) => {
      let data = await response.json();
      if (response.status === 200 || response.status === 201) {
        return data;
      }
      else {
        return { "error": `Response status ${response.status}` }
      }
    }).catch((error) => {
      return { "error": error };
    });


  }



  endpoint(resource = '', action = '', options = {}) {
    const existingEndpoint = this.endpoints[resource][action];

    if (existingEndpoint) {
      options = {
        ...options
      }
      const endpoint = existingEndpoint(options);
      return this.request(endpoint);
    } else {
      throw new Error(`Non existent endpoint "${action}"`)
    }
  }

}