import { ShaarliApi } from '../assets/js/shaarli.js'
import { default as Tagify } from '../assets/vendor/tagify.esm.js'
import { UtilsShaanti } from '../assets/js/utils_shaanti.js'

document.addEventListener("DOMContentLoaded", () => {

  const Shaanti = (() => {

    let instanceUrl;
    let token;
    let defaultPrivate;
    let currentTab;
    let shaarliApi;
    let utilsShaanti = new UtilsShaanti();
    let tagify;
    let editId;
    let runTime = (typeof browser === 'undefined') ? chrome : browser;

    const $tagsField = document.querySelector('.js-tags-field');
    const $descriptionField = document.querySelector('.js-description-field');
    const $urlField = document.querySelector('.js-url-field');
    const $titleField = document.querySelector('.js-title-field');
    const $urlExists = document.querySelector('.js-exists');
    const $linkForm = document.querySelector('.js-form');

    const $searchForm = document.querySelector('.js-form-search');
    const $searchInput = document.querySelector('.js-search-input');
    // const $searchTags = document.querySelector('.js-search-tags');
    const $searchResults = document.querySelector('.js-search-results');
    const $loaderMain = document.querySelector('.js-loading');

    const delegateEvents = () => {
      runTime.tabs.query({ currentWindow: true, active: true }).then((tabs) => {
        currentTab = tabs[0]; // Safe to assume there will only be one result
        $urlField.value = currentTab.url;
        $titleField.value = currentTab.title;
      }, console.error);

      // commands is not available on FF Android
      // https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/commands
      if(typeof runTime.commands !== 'undefined') {
        runTime.commands.onCommand.addListener((command) => {
          if (command === "open-search") {
            console.log('search');
            document.querySelector('.js-tab-button[data-tabname="search"]').click()
          }
        });
      }

      $linkForm.addEventListener('submit', submitForm);
      $searchForm.addEventListener('submit', submitSearchForm);
      $searchForm.addEventListener('keydown', (e) => {
        console.log(e);
        if ( e.key == 'Enter' ) {
          e.preventDefault();
          return false;
        } 
      });
      $searchInput.addEventListener('keydown', processInput);
     
      document.querySelectorAll('.js-ext-options').forEach($elm => $elm.addEventListener('click', goToOptions));

      tagify = new Tagify($tagsField, {
        enforceWhitelist: false,
        whitelist: [],
        delimiters: ',',
        dropdown: {
          enabled: 1,
          maxItems: 10
        }
      });

      restoreOptions();
    };

    const goToOptions = (click) => {
      let opening = runTime.runtime.openOptionsPage();
      opening.then(onOpened, onError);
    }

    const submitForm = (e) => {
      e.preventDefault();
      utilsShaanti.addLoadingSave();
      let formData = new FormData($linkForm);
      let formDataObj = Object.fromEntries(formData.entries());
      if (formDataObj.tags.length > 0) {
        formDataObj.tags = JSON.parse(formDataObj.tags).map(i => i.value)
      }
      formDataObj.private = (formDataObj.private) ? true : false;
      createUpdate(formDataObj);
    }

    const debounce = (func, timeout = 350) => {
      let timer;
      return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => { func.apply(this, args); }, timeout);
      };
    }

    const processInput = debounce((e) => {
      if (e.key == 'Enter' || e.key == 'Tab') {
        e.preventDefault();
        return false;
      }
      submitSearchForm()
    });

    const submitSearchForm = async (e) => {
      $searchResults.textContent = '';
      $loaderMain.classList.remove('hide');

      try {
        const data = await shaarliApi.endpoint('links', 'search', {
          "limit": 10,
          "searchterm": $searchInput.value,
        });

        if (!data.error && data.length > 0) {
          data.forEach((s) => {
            let $entry = document.createElement('a');
            let $entryTitle = document.createElement('div');
            $entry.target = '_blank';
            $entry.href = s.url;
            $entryTitle.textContent = s.title;
            $entry.appendChild($entryTitle);
            s.tags.forEach((t) => {
              let $tag = document.createElement('span');
              $tag.classList.add('tag-info');
              $tag.textContent = t;
              $entry.appendChild($tag);
            });
            $searchResults.appendChild($entry);
          });
        } else if (!data.error) {
          $searchResults.textContent = 'No results found'
        } else if (data.error) {
          utilsShaanti.showToast(false, `Error!! ${data.error}`);
        }

        $loaderMain.classList.add('hide');
      } catch (error) {
        $loaderMain.classList.add('hide');
        utilsShaanti.showToast(false, `Error!! ${error}`);
      }
    }

    const onOpened = () => {
      console.log(`Options page opened`);
    }

    const onError = (error) => {
      console.log(`Error: ${error}`);
    }

    const restoreOptions = () => {
      const setCurrentChoice = (result) => {
        if (result.instanceUrl && result.token) {
          instanceUrl = result.instanceUrl;
          token = result.token;
          defaultPrivate = result.defaultPrivateLinks;
          shaarliApi = new ShaarliApi(instanceUrl, token);
          document.querySelector('.js-private-field').checked = defaultPrivate;
          checkUrlExistance();
          getTags();
        } else {
          setOptionsFirst();
        }

      }

      const onError = (error) => {
        console.log(`Error: ${error}`);
      }

      let getting = runTime.storage.sync.get(['instanceUrl', 'token', 'defaultPrivateLinks']);
      getting.then(setCurrentChoice, onError);
    }

    const setOptionsFirst = () => {
      $linkForm.classList.add('hide');
      document.querySelectorAll('.js-tab-button').forEach($e => $e.classList.add('hide'));
      document.querySelector('.js-set-options-first').classList.remove('hide');
    }

    const checkUrlExistance = async () => {
      let metaDescription;
      $loaderMain.classList.remove('hide');
      try {
        metaDescription = await runTime.scripting.executeScript({
          target: {
            tabId: currentTab.id,
          },
          func: () => {
            let mDesc = document.querySelector('meta[name="description"]')?.content;
            mDesc = mDesc || document.querySelector('meta[property="og:description"]')?.content;
            mDesc = mDesc || document.querySelector('property[itemprop="og:description"]')?.content;
            mDesc = mDesc || ''
            return mDesc;

          },
        });
      } catch (err) {
        console.error(`failed to execute script: ${err}`);
      }

      metaDescription = metaDescription[0]?.result || '';

      try {
        const data = await shaarliApi.endpoint('links', 'search', {
          "searchterm": currentTab.url,
        });

        if (!data.error && data.length > 0) {
          if ($urlField.value == data[0].url) {
            editId = data[0].id;
            // runTime.action.setIcon({
            //   path: {
            //     16: "/assets/icons/star-16-fill.png",
            //     32: "/assets/icons/star-32-fill.png"
            //   }})
          }
          $tagsField.value = data[0].tags.toString();
          $descriptionField.value = data[0].description;
          $titleField.value = data[0].title;
          $urlExists.classList.remove('hide');
          $urlExists.href = `https://${instanceUrl}/shaare/${data[0].shorturl}`;
        } else {
          $descriptionField.value = metaDescription;
        }
        $loaderMain.classList.add('hide');
      } catch (error) {
        $loaderMain.classList.add('hide');
        console.log(error);
      }
    };

    const getTags = async (urlTags) => {
      $loaderMain.classList.remove('hide');
      try {
        const data = await shaarliApi.endpoint('tags', 'list', {});
        if (!data.error && data.length > 0) {
          tagify.whitelist = data.map(i => i.name);
        } else if (data.error) {
          utilsShaanti.showToast(false, `Error!! ${data.error}`);
        }
        $loaderMain.classList.add('hide');
      } catch (error) {
        console.log(error);
        utilsShaanti.showToast(false, `Error!! ${error}`);
        $loaderMain.classList.add('hide');
      }

    };

    const createUpdate = async (params) => {
      let update = (typeof editId === 'undefined') ? false : true;
      let data;
      try {
        if (update) {
          params.id = editId;
          data = await shaarliApi.endpoint('links', 'edit', {
            ...params
          });
        } else {
          data = await shaarliApi.endpoint('links', 'create', {
            ...params
          });
        }

        utilsShaanti.removeLoadingSave();
        if (data && !data.error && (data?.id || data?.id == 0)) {
          // runTime.action.setIcon({
          //   path: {
          //     16: "/assets/icons/star-16-fill.png",
          //     32: "/assets/icons/star-32-fill.png"
          //   }})
          window.close();
        } else {
          utilsShaanti.showToast(false, `Error!! ${data.error}`);
        };


      } catch (error) {
        console.log(error);
        utilsShaanti.removeLoadingSave();
      }


    };

    return {
      init: function () {
        delegateEvents();
      }
    };
  })();

  Shaanti.init();
});
